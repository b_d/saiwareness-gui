# README #

GUI for alarm system that runs on Raspberry Pi and logs all events that it detects in simple text files that are read, interpreted and visualized by the GUI. Sensors that are included in the alarm system are: gas, fire, illumination, temperature, humidity, water, sound and knock sensor. 


### Python modules used ###
* Tkinter
* Matplotlib