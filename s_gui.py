import matplotlib
matplotlib.use('TKAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as pltlib
import Tkinter
from Tkinter import *
from pylab import *
import numpy as np
from random import randint
import math
import datetime
import threading

class App_Window(Tkinter.Tk):
    def __init__(self,parent):
        Tkinter.Tk.__init__(self,parent)
        self.parent = parent
        self.list_length = 8
        self.initialize()
    
    def initialize(self):
        self.today = datetime.datetime.now()
        self.first_hours_hum_temp=[]
        self.first_hours_ill=[]
        self.reset_times()
        self.hours = ("00:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00")
        try:
            with open("conf.txt") as f:
                file = f.readlines()        
        except IOError:
            print "There is no config file in directory."
        i=0
        for red in file:
            if i == 1:
                self.timer_time = int(red[0:])
                break
            i=i+1     
        self.draw_timer = None
        
        btnAbout = Tkinter.Button(self,text="About",command=self.about).grid(row=4, column=6, columnspan =1 )
        btnExit = Tkinter.Button(self,text="Exit",command=self._quit).grid(row=4, column=7, columnspan =1 )
        
        self.lb_sound = Tkinter.Listbox(self,height=self.list_length, width=25)
        self.lb_sound.grid(row=3, column=0, columnspan =2 )
        self.lb_fire = Tkinter.Listbox(self,height=self.list_length, width=25)
        self.lb_fire.grid(row=3, column=2, columnspan =2 )
        self.lb_gas = Tkinter.Listbox(self,height=self.list_length, width=25)
        self.lb_gas.grid(row=3, column=4, columnspan =2 )
        self.lb_knock = Tkinter.Listbox(self,height=self.list_length, width=25)
        self.lb_knock.grid(row=3, column=6, columnspan =2 )
        
        Tkinter.Label(self, text="Sound alert").grid(row=2, column=0, columnspan = 2)
        Tkinter.Label(self, text="Fire/Water alert").grid(row=2, column=2, columnspan = 2)
        Tkinter.Label(self, text="Gas alert").grid(row=2, column=4, columnspan = 2)
        Tkinter.Label(self, text="Knock alert").grid(row=2, column=6, columnspan = 2)        
        
        self.btn_day_before = Tkinter.Button(self,text="<",command=self.day_before)
        self.btn_day_before.grid(row=4, column=0, columnspan = 1)

        self.btn_day_after = Tkinter.Button(self,text=">",command=self.day_after)
        self.btn_day_after.grid(row=4, column=2, columnspan = 1)
        self.btn_day_after.config(state=DISABLED)
        self.lbl_date = Tkinter.Label(self, text=self.today.strftime("%d/%m/%Y"))
        self.lbl_date.grid(row=4, column=1, columnspan = 1)        
        
        self.btn_goto_today = Tkinter.Button(self,text="Now",command=self.GotoToday)
        self.btn_goto_today.grid(row=4, column=5, columnspan = 1)
        
        self.protocol("WM_DELETE_WINDOW", self._quit)    
        self.canvasFig=pltlib.figure(1)
        Fig = matplotlib.figure.Figure(figsize=(7,4),dpi=138)
        self.FigSubPlot1 = Fig.add_subplot(311)     
        self.FigSubPlot1.set_title("Humidity")     
        self.FigSubPlot2 = Fig.add_subplot(312)          
        self.FigSubPlot2.set_title("Temperature")
        self.FigSubPlot3 = Fig.add_subplot(313)          
        self.FigSubPlot3.set_title("Illumination")
        x=[]
        y=[]
        
        self.hum_line, = self.FigSubPlot1.plot(x,y,'k',linewidth=0.3)
        self.temp_line, = self.FigSubPlot2.plot(x,y,'k',linewidth=0.3)
        self.ill_line, = self.FigSubPlot3.plot(x,y,'k',linewidth=0.3)
        self.sc1 = None
        self.sc2 = None
        self.sc3 = None
        Fig.tight_layout()    
        self.canvas = matplotlib.backends.backend_tkagg.FigureCanvasTkAgg(Fig, master=self)
        self.canvas.show()
        self.canvas.get_tk_widget().grid(row=0, column=0, columnspan = 8)
        self.canvas._tkcanvas.grid(row=0, column=0, columnspan = 8)
        
        font = {
        'weight' : 'bold',
        'size'   : 7}
        matplotlib.rc('font', **font)

        self.resizable(True,False)
        self.title("SaiwarenesS v1.2")
        self.update()
        self.updateGraph()
    
    def updateGraph(self):        
        file_name = self.today.strftime("%Y%m%d")
        self.reset_times()
        try:
            with open(file_name + "_H.txt") as f:
                content_H = f.readlines()
        except IOError:
            content_H = ""

        humX = []
        humY = []
        tempX = []
        tempY = []
        
        i=0
        for row in content_H:
            if(self.first_hours_hum_temp[int(row[11:13])] == "Q"):
                self.first_hours_hum_temp[int(row[11:13])] = i
            index = row.find(';')
            humY.append(float(row[index+12:index+15]))
            humX.append(i)
            tempY.append(float(row[index+33:index+36]))
            tempX.append(i)
            i=i+1
        
        try:
            with open(file_name + "_I.txt") as f:
                content_I = f.readlines()        
        except IOError:
            content_I = ""
            
        illuminationX = []
        illuminationY = []
        i=0
        for row in content_I:
            if(self.first_hours_ill[int(row[11:13])] == "Q"):
                self.first_hours_ill[int(row[11:13])] = i
            index = row.find(';')
            illuminationX.append(i)
            illuminationY.append(float(row[index+24:]))
            i=i+1

        sounds=[]
        fires=[]
        gases=[]
        knockes=[]
        try:
            exists = True
            alarm_file = open(file_name + "_A.txt")
        except IOError:
            exists = False             
            self.delete_lb()
            
        if exists:
            for line in reversed(alarm_file.readlines()):
                l = line.rstrip()
                date = l[0:19]
                index = l.find(';')
                if(l[index+2] == 'S' and len(sounds) < self.list_length):
                    sounds.append(date + " - " + l[index+26:])    
                elif((l[index+2] == 'F' or l[index+2] == 'W') and len(fires) < self.list_length):
                    if l[index+2] == 'F':
                        fires.append(date + ' - Fire')
                    elif l[index+2] == 'W':
                        fires.append(date + ' - Water')
                elif(l[index+2] == 'G' and len(gases) < self.list_length):
                    gases.append(date + " - " + l[index+30:])
                elif(l[index+2] == 'K' and len(knockes) < self.list_length):
                    knockes.append(date)
                if(len(sounds) == self.list_length and len(fires) == self.list_length and len(gases) == self.list_length and len(knockes) == self.list_length):                
                    break                
            self.delete_lb()
            
            for sound in sounds:
                self.lb_sound.insert(END,sound)
            for fire in fires:
                self.lb_fire.insert(END,fire)  
            for gas in gases:
                self.lb_gas.insert(END,gas)    
            for knock in knockes:
                self.lb_knock.insert(END,knock)  
        self.refreshFigure(humX,humY,tempX,tempY,illuminationX,illuminationY)
        self.draw_timer = threading.Timer(self.timer_time , self.updateGraph)
        self.draw_timer.start()

    def refreshFigure(self,x1,y1,x2,y2,x3,y3):  
                
        if(self.sc1 != None):
            self.sc1.remove()
        if(self.sc2 != None):
            self.sc2.remove()
        if(self.sc2 != None):
            self.sc3.remove()
        self.hum_line.set_data(x1,y1)
        self.temp_line.set_data(x2,y2)
        self.ill_line.set_data(x3,y3)
        ax = self.canvas.figure.axes[0]

        if(x1 == []):
            x_min = -1
            x_max = 1
        else:
            x_min = min(x1) - 1
            x_max = max(x1) + 1
        if(y1 == []):
            y_min = -1
            y_max = 1
        else:
            y_min = min(y1) - 1
            y_max = max(y1) + 1
        ax.set_xlim([x_min, x_max])
        ax.set_ylim([y_min, y_max])
        ax.set_xticks(x1)
        labels =['' for i in x1]
        for i in range(0,len(self.first_hours_hum_temp)):
            if(self.first_hours_hum_temp[i] != "Q"):
                labels[self.first_hours_hum_temp[i]] = self.hours[i]
        ax.set_xticklabels(labels, rotation=20, fontsize=4)
        self.sc1 = self.FigSubPlot1.scatter(x1,y1,s=1, color='row')
        
        ax = self.canvas.figure.axes[1]
        if(x2 == []):
            x_min = -1
            x_max = 1
        else:
            x_min = min(x2) - 1
            x_max = max(x2) + 1
        if(y2 == []):
            y_min = -1
            y_max = 1
        else:
            y_min = min(y2) - 1
            y_max = max(y2) + 1
        ax.set_xlim(x_min, x_max)
        ax.set_ylim(y_min, y_max)
        ax.set_xticks(x1)
        ax.set_xticklabels(labels, rotation=20, fontsize=4)
        self.sc2 = self.FigSubPlot2.scatter(x2,y2,s=1, color='row')
        
        ax = self.canvas.figure.axes[2]
        
        if(x3 == []):
            x_min = -1
            x_max = 1
        else:
            x_min = min(x3) - 1
            x_max = max(x3) + 1
        if(y3 == []):
            y_min = -1
            y_max = 1
        else:
            y_min = min(y3) - 1
            y_max = 220
        ax.set_xlim(x_min, x_max)
        ax.set_ylim(y_min, y_max) 
        ax.set_xticks(x3)
        labels =[]
        labels =['' for i in x3]
        for i in range(0,len(self.first_hours_ill)):
            if(self.first_hours_ill[i] != "Q"):
                labels[self.first_hours_ill[i]] = self.hours[i]
        ax.set_xticklabels(labels, rotation=20, fontsize=4)     
        self.sc3 = self.FigSubPlot3.scatter(x3,y3,s=1, color='row')
        
        self.canvas.draw()

    def reset_times(self):
        self.first_hours_hum_temp = []
        self.first_hours_ill = []
        for i in range(24):
            self.first_hours_hum_temp.append("Q")
            self.first_hours_ill.append("Q")

    def day_before(self):
        yesterday = self.today - datetime.timedelta(days=1)        
        if self.file_exists(yesterday):
            self.today = yesterday
            self.lbl_date.config(text=self.today.strftime("%d/%m/%Y"))
            self.btn_day_after.config(state=NORMAL)
            self.draw_timer.cancel()
            self.updateGraph()
        else:
            self.btn_day_before.config(state=DISABLED)

    def day_after(self):
        tomorrow = self.today + datetime.timedelta(days=1)        
        if self.file_exists(tomorrow):
            self.today = tomorrow
            self.lbl_date.config(text=self.today.strftime("%d/%m/%Y"))
            self.btn_day_before.config(state=NORMAL)
            self.draw_timer.cancel()
            self.updateGraph()
        else:
            self.btn_day_after.config(state=DISABLED)

    def file_exists(self, dan):
        exists = False
        try:
            alarm_file = open(dan.strftime("%Y%m%d") + "_A.txt")
            exists = True
        except IOError:
            pass
        try:
            alarm_file = open(dan.strftime("%Y%m%d") + "_H.txt")
            exists = True
        except IOError:
            pass
        try:
            alarm_file = open(dan.strftime("%Y%m%d") + "_I.txt")
            exists = True
        except IOError:
            pass
        return exists

    def about(self):
        popup = Tkinter.Tk()
        popup.wm_title("About")
        explanation = """
        SaiwareneS v1.2, March 2015.
        
        Saipem's IoT Environment Awareness System
        
        Project management: Sasa Aksentijevic
        Technical management: David Krnjak
        Coding: Dino Bikic"""
        w2 = Tkinter.Label(popup, 
           justify=LEFT,
           padx = 10, 
           text=explanation).pack()
        
        B1 = Tkinter.Button(popup, text="Close", command = popup.destroy)
        B1.pack()
        popup.mainloop()


    def GotoToday(self):         
        self.today = datetime.datetime.now()
        self.lbl_date.config(text=self.today.strftime("%d/%m/%Y"))
        self.btn_day_before.config(state=NORMAL)
        self.btn_day_after.config(state=DISABLED)
        self.draw_timer.cancel()
        self.updateGraph()
        
    def delete_lb(self):
        self.lb_sound.delete(0,self.list_length)
        self.lb_fire.delete(0,self.list_length)
        self.lb_gas.delete(0,self.list_length)
        self.lb_knock.delete(0,self.list_length)
        
    def _quit(self):
        self.draw_timer.cancel()        
        self.quit()
        self.destroy()
if __name__ == "__main__":
    MainWindow = App_Window(None)
    MainWindow.mainloop()